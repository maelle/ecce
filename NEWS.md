
# ecce 1.0.9

### Major Change

* Modify the README.
* Modify the LICENSE to 2022.

# ecce 1.0.8

### Major Change

* The translate_full function output is more concise.
* LazyData field value is changed to false

# ecce 1.0.7

### Fix Bug

* Specification of Description information in accordance with CRAN official requirements.

# ecce 1.0.6

### New Features

* Adjust URL order in DESCRIPTION file about URL field.
* Further tests on Linux found no problems.

# ecce 1.0.5

### Fix Bug

* Fix install_gitlab error case.
* Change the notes to English just in case.
* Add ecce url in DESCRIPTION file about URL field.

# ecce 1.0.4

### Fix Bug

* Fix "The Title field should be in title case" in DESCRIPTION file.
* DESCRIPTION file remove redundant Link field.
* Add vignettes in .tar.gz deal with "no prebuilt vignette index"
* Modify DESCRIPTION file Description information.

# ecce 1.0.3

### Fix Bug

* R/.R file contain Chinese characters, this maybe lead to errors when load ecce package.
* DESCRIPTION file add a Link to ecce website.

# ecce 1.0.2

### Fix Bug

* Fix test file contain Chinese characters compile error.
* DESCRIPTION file add Imports:stringr, modify Depends:R (>= 3.5.0).
* Note: compile vignettes need R (>= 3.6.2).

# ecce 1.0.1

### New Features

* Add function `translate_full`, which can display more information.
* Add function `translate_view`, through this function you can get the entire web page information.
* Update a new logo for ecce package.

# ecce 1.0.0

### New Features

* First public release.
